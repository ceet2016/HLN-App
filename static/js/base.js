// 开启原生H5的GPU硬件加速
function plusReady() {
	create();
}

if(window.plus) {
	plusReady();
} else {
	document.addEventListener('plusready', plusReady, false);
}

// 创建新窗口并设置开启硬件加速
function create() {
	var styles = {};
	// 在Android5以上设备，如果默认没有开启硬件加速，则强制设置开启
	if(!plus.webview.defaultHardwareAccelerated() && parseInt(plus.os.version) >= 5) {
		styles.hardwareAccelerated = true;
	}
}

// 判断是否是微信浏览器的函数
function isWeiXin() {
	var ua = window.navigator.userAgent.toLowerCase();
	if(ua.match(/MicroMessenger/i) == 'micromessenger') {
		return true;
	} else {
		return false;
	}
}

window.onload = function() {
	var tip = document.getElementById('weixin-tip-box');
	if(isWeiXin()) {
		// 是微信浏览器，执行操作
		tip.style.display = 'block';
	} else {
		return false;
	}
}
// 安卓机返回机制
document.addEventListener('plusready', function() {
	var webview = plus.webview.currentWebview();
	plus.key.addEventListener('backbutton', function() {
		webview.canBack(function(e) {
			var filename = location.href;
			filename = filename.substr(filename.lastIndexOf('/') + 1);
			if(filename == "Login" || filename == "" || filename == "Find" || filename == "My") {
				if(window.confirm('是否退出？')) {
					webview.close();
					return true;
				} else {
					return false;
				}
			} else {
				webview.back();
			}
		})
	});
});