'use strict';
const path = require('path');

module.exports = {
	dev: {
		assetsSubDirectory: 'static', // 静态资源要存放的路径
		assetsPublicPath: '/', // 引用资源的的相对地址
		proxyTable: { // 运行时代理
			'/api': {
				target: 'http://192.168.0.35:8099',
				changeOrigin: true,
				pathRewrite: {
					'^/api': ''
				}
			}
		},
		host: '0.0.0.0', // 默认地址 0.0.0.0
		port: 8080, // 默认端口 8080
		autoOpenBrowser: false, // 是否自动打开
		errorOverlay: true, // 是否展示错误的通知
		notifyOnErrors: true,
		poll: false, // 获取文件更改的通知
		useEslint: true, // ESlint语法检验
		showEslintErrorsInOverlay: false, // 不符合Eslint规则时只警告(默认运行出错)
		devtool: 'eval-source-map',
		cacheBusting: true, // 强制浏览器下载新文件
		cssSourceMap: false // 记录压缩的翻译文件，通过这个文件可以找到对应的源码
	},

	build: {
		index: path.resolve(__dirname, '../dist/index.html'), // 打包的首页
		assetsRoot: path.resolve(__dirname, '../dist'), // 打包文件的目录
		assetsSubDirectory: 'static', // 打包后静态资源文件夹名称
		assetsPublicPath: './',
		productionSourceMap: false,
		devtool: '#source-map',
		// 开启 Gzip 压缩
		productionGzip: false, // app 是 false ,wap 是 true
		productionGzipExtensions: ['js', 'css'],
		bundleAnalyzerReport: process.env.npm_config_report
	}
}