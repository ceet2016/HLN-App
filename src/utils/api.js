import { get, post } from './http';

/**
 * @param {Base} [Base Api]
 */
export const base = {
	/**
	 * 登录
	 * @param {POST} Method [请求的类型]
	 */
	getSignUp(res) {
		return post(process.env.API_HOST + 'login', res);
	},
	/**
	 *  退出登录
	 * @param {POST} Method [请求的类型]
	 */
	getSignOut(res) {
		return post(process.env.API_HOST + 'user/logout', res);
	},
	/**
	 *  注册
	 * @param {POST} Method [请求的类型]
	 */
	getRegeist(res) {
		return post(process.env.API_HOST + 'user/register', res);
	},
	/**
	 *  忘记密码
	 * @param {POST} Method [请求的类型]
	 */
	getForgotPassword(res) {
		return post(process.env.API_HOST + 'user/forgotPassword', res);
	},
	/**
	 *  检测是否设置交易密码
	 * @param {GET} Method [请求的类型]
	 */
	getIsTradePass(res) {
		return get(process.env.API_HOST + 'user/getIsTradePass', res);
	},
	/**
	 *  设置交易密码
	 * @param {POST} Method [请求的类型]
	 */
	setTradePassWord(res) {
		return post(process.env.API_HOST + 'user/setTradePassWord', res);
	},
	/**
	 *  修改交易密码
	 * @param {POST} Method [请求的类型]
	 */
	upTradePassWrod(res) {
		return post(process.env.API_HOST + 'user/upTradePassWrod', res);
	},
	/**
	 *  修改密码
	 * @param {POST} Method [请求的类型]
	 */
	upPassword(res) {
		return post(process.env.API_HOST + 'user/upPassword', res);
	},
	/**
	 *  获取用户信息
	 * @param {GET} Method [请求的类型]
	 */
	getUser_info(res) {
		return get(process.env.API_HOST + 'user/user_info', res);
	},
	/**
	 *  获取用户账户相关接口，可用、冻结余额等信息
	 * @param {GET} Method [请求的类型]
	 */
	getMyInfo(res) {
		return get(process.env.API_HOST + 'my/myInfo', res);
	},
	/**
	 *  获取联系客服
	 * @param {GET} Method [请求的类型]
	 */
	getCustomer_service(res) {
		return get(process.env.API_HOST + 'my/customer_service', res);
	},
	/**
	 *  我的分享-邀请码
	 * @param {GET} Method [请求的类型]
	 */
	getSharing(res) {
		return get(process.env.API_HOST + 'my/sharing', res);
	},
	/**
	 *  我的分享-分享人数
	 * @param {GET} Method [请求的类型]
	 */
	getMyteam(res) {
		return get(process.env.API_HOST + 'my/myteam', res);
	},
	/**
	 *  我的分享-当前算力值
	 * @param {GET} Method [请求的类型]
	 */
	getCalculation(res) {
		return get(process.env.API_HOST + 'my/calculation', res);
	},
	/**
	 *  公告-分页查询
	 * @param {GET} Method [请求的类型]
	 */
	getNoticeservice(res) {
		return get(process.env.API_HOST + 'my/noticeservice', res);
	},
	/**
	 *  账单查询
	 * @param {POST} Method [请求的类型]
	 */
	getAccountFlow(res) {
		return post(process.env.API_HOST + 'my/account_flow', res);
	},
	/**
	 *  获取分享奖励
	 * @param {GET} Method [请求的类型]
	 */
	getShareReward(res) {
		return get(process.env.API_HOST + 'my/accountFlow/shareReward', res);
	},
	/**
	 *  获取分享奖励
	 * @param {GET} Method [请求的类型]
	 */
	getShareRewardDetail(res) {
		return get(process.env.API_HOST + 'my/accountFlow/shareRewardDetail', res);
	},
	/**
	 *  获取账户余额
	 * @param {GET} Method [请求的类型]
	 */
	getUserBalance(res) {
		return get(process.env.API_HOST + 'user/transfer/balance/1', res);
	},
	/**
	 * 提币按钮
	 * @param {POST} Method [请求的类型]
	 */
	getTransfer(res) {
		return post(process.env.API_HOST + 'user/transfer', res);
	},
	/**
	 * 获取凭证
	 * @param {GET} Method [请求的类型]
	 */
	getCredentials(res) {
		return get(process.env.API_HOST + 'support/rsa', res);
	},
}

/**
 * @param {Information} [Information Api]
 */
export const information = {
	/**
	 *  资讯-分页查询
	 * @param {GET} Method [请求的类型]
	 */
	getJournalismservice(res) {
		return get(process.env.API_HOST + 'findtype/journalismservice', res);
	}
}

/**
 * @param {Home} [Home Api]
 */
export const home = {
	/**
	 *  红包-我的幸运红包
	 * @param {GET} Method [请求的类型]
	 */
	getLottery(res) {
		return get(process.env.API_HOST + 'pool/lottery', res);
	},
	/**
	 *  红包-拆开
	 * @param {POST} Method [请求的类型]
	 */
	getLottery2(res) {
		return post(process.env.API_HOST + 'pool/lottery', res);
	},
	/**
	 *  共享算池列表
	 * @param {GET} Method [请求的类型]
	 */
	getSharedPools(res) {
		return get(process.env.API_HOST + 'pool/getSharedPools', res);
	},
	/**
	 *  共享算池详情
	 * @param {GET} Method [请求的类型]
	 */
	getSharedPoolDetail(res) {
		return get(process.env.API_HOST + 'pool/getSharedPoolDetail', res);
	},
	/**
	 *  退出共享算池
	 * @param {POST} Method [请求的类型]
	 */
	exitSharedPool(res) {
		return post(process.env.API_HOST + 'pool/exitSharedPool', res);
	},
	/**
	 *  共享算池
	 * @param {GET} Method [请求的类型]
	 */
	getSharedPool(res) {
		return get(process.env.API_HOST + 'pool/getSharedPool', res);
	},
	/**
	 *  参与共享算池
	 * @param {POST} Method [请求的类型]
	 */
	joinSharedPool(res) {
		return post(process.env.API_HOST + 'pool/joinSharedPool', res);
	},
	/**
	 *  获取收益列表
	 * @param {GET} Method [请求的类型]
	 */
	getSharedPoolDetailList(res) {
		return get(process.env.API_HOST + 'pool/getSharedPoolDetailList', res);
	},
	/**
	 *  获取收益介绍
	 * @param {GET} Method [请求的类型]
	 */
	getIncomeProfile(res) {
		return get(process.env.API_HOST + 'pool/incomeProfile', res);
	},
	/**
	 *  下单
	 * @param {POST} Method [请求的类型]
	 */
	ipdPoolOrder(res) {
		return post(process.env.API_HOST + 'pool/ipdPoolOrder', res);
	},
	/**
	 *  获取下单周期
	 * @param {GET} Method [请求的类型]
	 */
	getOrderDay(res) {
		return get(process.env.API_HOST + 'global_config/configs/GLOBAL_COIN_MINIMUM_DAYS', res);
	},
	/**
	 *  获取密码服务信息
	 * @param {GET} Method [请求的类型]
	 */
	getConfirmPassword(res) {
		return get(process.env.API_HOST + 'global_config/configs/GLOBAL_CONFIRM_PASSWORD', res);
	},
	/**
	 *  独立算池参与记录
	 * @param {POST} Method [请求的类型]
	 */
	myParticipation(res) {
		return post(process.env.API_HOST + 'pool/myParticipation', res);
	},
}