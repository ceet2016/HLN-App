/**
 * 用不到的组件请注释掉，不要放开
 */
import router from './router';
import App from './App';

Vue.config.productionTip = false;

// 安装 vant 主框架
const vant = require('vant');
import '@/assets/styles/reset.less';

// 函数式编程组件
const _ = require('lodash');

// axios 以及相关处理代码
Vue.prototype.axios = axios;
Vue.prototype.qs = Qs;

// 多语言
const VueI18n = require('vue-i18n');
let leng;
if(localStorage.leng == 1 || localStorage.leng == undefined){
  leng = 'cn';
  axios.defaults.headers.common['Language'] = 'zh_CN';
}else if(localStorage.leng == 2){
  leng = 'en';
  axios.defaults.headers.common['Language'] = 'en_US';
}
const i18n = new VueI18n({
  locale: leng || 'cn', //定义默认语言
  messages:{
    'cn': require('../static/lang/cn'), // 中文语言包
    'en': require('../static/lang/en') // 英文语言包
  }
})

// 页面权限
router.beforeEach((to, from, next) => {
	if(to.matched.some(item => item.meta.requiresAuth)) {
		// 当 token 存在证已经登录，设置登录后的请求头，否则进入登录页面
		if(sessionStorage.token) {
			axios.defaults.headers['Authorization'] = 'bearer ' + sessionStorage.token;
			next();
		} else {
			next({
				path: '/Login'
			});
		}
	} else {
		if(to.path == '/Register' || to.path == '/FindPwd' || to.path == '/Login') {
			axios.defaults.headers['Authorization'] = '';
		}
		next();
	}
	// 响应拦截（配置请求回来的信息）
	axios.interceptors.response.use(function(response) { // 处理响应数据
		// 判断如果请求返回1004 3301 4004 5501状态码，即登录超时，清除token并跳回登录页
		if(response.data.code == '1004' || response.data.code == '3301' || response.data.code == '5501') {
			vant.Notify({
				message: response.data.msg,
				duration: 1000,
				background: '#1989fa'
			});
			sessionStorage.clear();
			next({
				path: '/Login'
			});
		}
		return response;
	}, function(error) { // 处理响应失败
		setTimeout(() => {
			if(error.response.status == '404') {
				vant.Notify({
					message: '404 请求接口地址错误',
					duration: 1000,
					background: '#1989fa'
				});
			} else if(error.response.status == '405') {
				vant.Notify({
					message: '405 请求类型错误',
					duration: 1000,
					background: '#1989fa'
				});
			} else if(error.response.status == '500') {
				vant.Notify({
					message: error.response.data.msg,
					duration: 1000,
					background: 'rgb(255, 68, 68)'
				});
			} else if(error.response.status == '504') {
				vant.Notify({
					message: '504 网关错误',
					duration: 1000,
					background: 'rgb(255, 68, 68)'
				});
			} else {
				vant.Notify({
					message: error.response.status + ' 其他错误：' + error.response.data.msg,
					duration: 1000,
					background: 'rgb(255, 68, 68)'
				});
			}
		}, 1000);
		return Promise.reject(error);
	});
});

// 加密
// 正常加密
Vue.prototype.rsaEncrypt = function(text) {
	let pubkey =
		"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCxnhv0xsWaLUc+WZleQFHznWbDUeWHeRVH9lClgm5SVUjCAGuD1GZHZlrojMrhwG65eeYM1NtmhZ96nwzL2OV5SgxFgEscHjbExdvVNckw4mYUlyL8QyhnSEQvgHUokcQcBt9oJZMajnmwyDbPTFOUxmTnongFksrbTeJf6XUW5QIDAQAB";
	// java后台生成的
	let encrypt = new JSEncrypt();
	encrypt.setPublicKey(pubkey);
	// 加密
	return encrypt.encrypt(text)
};
// 超长加密
Vue.prototype.encryptLong = function(text) {
	let pubkey =
		"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCxnhv0xsWaLUc+WZleQFHznWbDUeWHeRVH9lClgm5SVUjCAGuD1GZHZlrojMrhwG65eeYM1NtmhZ96nwzL2OV5SgxFgEscHjbExdvVNckw4mYUlyL8QyhnSEQvgHUokcQcBt9oJZMajnmwyDbPTFOUxmTnongFksrbTeJf6XUW5QIDAQAB";
	// java后台生成的
	let encrypt = new JSEncrypt();
	encrypt.setPublicKey(pubkey);
	// 加密
	return encrypt.encryptLong(text)
};

/* eslint-disable no-new */
new Vue({
	router,
	i18n,
	render: h => h(App)
}).$mount('#app-box')