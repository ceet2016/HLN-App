import Index from '@/components/Index';
import Home from '@/components/Home';

const Router = require('vue-router');

export default new Router({
	mode: 'hash',
	linkActiveClass: 'on',
	routes: [{
		path: '/',
		component: Index,
		children: [{
			path: '',
			name: 'Home',
			component: Home,
			meta: {
				requiresAuth: true,
				title: '首页'
			}
		}, {
			path: '/OTC',
			name: 'OTC',
			component: () =>
				import('@/components/OTC'),
			meta: {
				requiresAuth: true,
				title: 'OTC'
			}
		}, {
			path: '/Information',
			name: 'Information',
			component: () =>
				import('@/components/Information'),
			meta: {
				requiresAuth: true,
				title: '资讯'
			}
		}, {
			path: '/Mine',
			name: 'Mine',
			component: () =>
				import('@/components/Mine'),
			meta: {
				requiresAuth: true,
				title: '我的'
			}
		}]
	}, {
		path: '/Login',
		name: 'Login',
		component: () =>
			import('@/components/Login'),
		meta: {
			title: '登录'
		}
	}, {
		path: '/Register',
		name: 'Register',
		component: () =>
			import('@/components/Register'),
		meta: {
			title: '注册'
		}
	}, {
		path: '/Forget',
		name: 'Forget',
		component: () =>
			import('@/components/Forget'),
		meta: {
			title: '忘记密码'
		}
	}, {
		path: '/Details',
		name: 'Details',
		component: () =>
			import('@/components/Details'),
		meta: {
			requiresAuth: true,
			title: '资讯详情'
		}
	}, {
		path: '/News',
		name: 'News',
		component: () =>
			import('@/components/News'),
		meta: {
			requiresAuth: true,
			title: '消息'
		}
	}, {
		path: '/NewsDetails',
		name: 'NewsDetails',
		component: () =>
			import('@/components/NewsDetails'),
		meta: {
			requiresAuth: true,
			title: '消息详情'
		}
	}, {
		path: '/UpdateMine',
		name: 'UpdateMine',
		component: () =>
			import('@/components/UpdateMine'),
		meta: {
			requiresAuth: true,
			title: '修改昵称'
		}
	}, {
		path: '/Share',
		name: 'Share',
		component: () =>
			import('@/components/Share'),
		meta: {
			requiresAuth: true,
			title: '我的分享'
		}
	}, {
		path: '/Setting',
		name: 'Setting',
		component: () =>
			import('@/components/Setting'),
		meta: {
			requiresAuth: true,
			title: '个人设置'
		}
	}, {
		path: '/UpdatePayPwd',
		name: 'UpdatePayPwd',
		component: () =>
			import('@/components/UpdatePayPwd'),
		meta: {
			requiresAuth: true,
			title: '修改交易密码'
		}
	}, {
		path: '/UpdatePwd',
		name: 'UpdatePwd',
		component: () =>
			import('@/components/UpdatePwd'),
		meta: {
			requiresAuth: true,
			title: '修改登录密码'
		}
	}, {
		path: '/SetPayPwd',
		name: 'SetPayPwd',
		component: () =>
			import('@/components/SetPayPwd'),
		meta: {
			requiresAuth: true,
			title: '设置交易密码'
		}
	}, {
		path: '/Recharge',
		name: 'Recharge',
		component: () =>
			import('@/components/Recharge'),
		meta: {
			requiresAuth: true,
			title: '区块充值'
		}
	}, {
		path: '/Extract',
		name: 'Extract',
		component: () =>
			import('@/components/Extract'),
		meta: {
			requiresAuth: true,
			title: '提币'
		}
	}, {
		path: '/Qr',
		name: 'Qr',
		component: () =>
			import('@/components/Qr'),
		meta: {
			requiresAuth: true,
			title: '扫一扫'
		}
	}, {
		path: '/billManagement',
		name: 'billManagement',
		component: () =>
			import('@/components/billManagement'),
		meta: {
			requiresAuth: true,
			title: '账单管理'
		}
	}, {
		path: '/billDetails',
		name: 'billDetails',
		component: () =>
			import('@/components/billDetails'),
		meta: {
			requiresAuth: true,
			title: '账单详情页'
		}
	}, {
		path: '/IndependentPool',
		name: 'IndependentPool',
		component: () =>
			import('@/components/IndependentPool'),
		meta: {
			requiresAuth: true,
			title: '独立算池'
		}
	}, {
		path: '/SharePool',
		name: 'SharePool',
		component: () =>
			import('@/components/SharePool'),
		meta: {
			requiresAuth: true,
			title: '共享算池'
		}
	}, {
		path: '/IndependentDetails',
		name: 'IndependentDetails',
		component: () =>
			import('@/components/IndependentDetails'),
		meta: {
			requiresAuth: true,
			title: '参与详情'
		}
	}, {
		path: '/MySharePool',
		name: 'MySharePool',
		component: () =>
			import('@/components/MySharePool'),
		meta: {
			requiresAuth: true,
			title: '我的参与'
		}
	}, {
		path: '/SharePoolDetails',
		name: 'SharePoolDetails',
		component: () =>
			import('@/components/SharePoolDetails'),
		meta: {
			requiresAuth: true,
			title: '共享奖池详情'
		}
	}, {
		path: '/SharePoolSettlement',
		name: 'SharePoolSettlement',
		component: () =>
			import('@/components/SharePoolSettlement'),
		meta: {
			requiresAuth: true,
			title: '共享奖池结算'
		}
	}
	]
})